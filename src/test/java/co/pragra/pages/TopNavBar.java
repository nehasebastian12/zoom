package co.pragra.pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import org.testng.annotations.AfterSuite;


public class TopNavBar {

private WebDriver driver;

@FindBy(xpath = "/html/body/div[1]/div[2]/div/div[1]/div[1]/div/ul/li[1]/a")
private WebElement reqDemo;

@FindBy(xpath = "/html/body/div[1]/div[2]/div/div[1]/div[1]/div/ul/li[2]/a")
private WebElement one800;

@FindBy(xpath = "//*[@id=\"btnResouces\"]")
private WebElement resources;

@FindBy(xpath = "/html/body/div[1]/div[2]/div/div[1]/div[1]/div/ul/li[4]/a")
private WebElement support;

@FindBy(xpath = "/html/body/div[1]/div[2]/div/div[1]/div[1]/div/ul/li[3]/ul/li[3]/a")
private WebElement videotut;

@FindBy(xpath="/html/body/div[1]/div[3]/div[1]/div[2]/div/div[2]/div[5]/div/div[1]")
private WebElement download;

public TopNavBar(WebDriver driver)
        {
        this.driver=driver;
        PageFactory.initElements(driver,this);
        }
public VideoPage getToVideo()
        {

        Actions actions=new Actions(driver);
        actions.moveToElement(resources).pause(2000).moveToElement(videotut).click().build().perform();
        return new VideoPage(driver);
        }



        @AfterSuite

        public void tearDown () throws InterruptedException {

                driver.quit();
        }}