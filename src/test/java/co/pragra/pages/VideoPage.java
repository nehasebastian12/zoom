package co.pragra.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class VideoPage {
    WebDriver driver;
    @FindBy(id="query")
    WebElement search;

    public VideoPage(WebDriver driver)
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }
    public VideoPage keyinSearchText(String text)
    {
        this.search.sendKeys(text);
        return  this;
    }
}
